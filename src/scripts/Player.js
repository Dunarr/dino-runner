import playerConfig from "../config/player.json"
import spritesheet from "/imgs/spritesheet.png"
import AssetManager from "./AssetManager";
import Animation from "./Animation";

export default class Player {
    x
    y
    moovingLeft
    width
    moovingRight
    constructor(screenHeight) {
        this.animation = new Animation(AssetManager.File(spritesheet), playerConfig)
        this.x = 48 - playerConfig.width/2
        this.y = screenHeight-8-playerConfig.height
        this.width = playerConfig.width
        this.moovingLeft = false
        this.moovingRight = false
        document.addEventListener("touchstart", this.handleTouch.bind(this))
        document.addEventListener("touchmove", this.handleTouch.bind(this))
        document.addEventListener("touchend", this.handleTouch.bind(this))
    }
    update(delta){
        this.animation.update(delta)
        this.updatePos(delta)
    }
    draw(ctx){
        this.animation.draw(ctx, this.x, this.y)
    }
    handleTouch({touches}){
        this.moovingLeft = false
        this.moovingRight = false
        for (let i = 0; i < touches.length; i++) {
            const touch = touches[i]
            let posX = touch.clientX / window.innerWidth < .5
            if (posX) {
                this.moovingLeft = true
            } else {
                this.moovingRight = true
            }
        }

    }
    updatePos(delta){
        let posX = this.x
        if(this.moovingLeft){
            posX-=delta*0.3
        }
        if(this.moovingRight){
            posX+=delta*0.3
        }
        if(posX+playerConfig.width > 80){
            posX = 80-playerConfig.width
        }
        if(posX<16){
            posX = 16
        }
        this.x = posX
    }
}
