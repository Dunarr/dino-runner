export default class AssetManager {
    _files
    static I
    constructor() {
        AssetManager.I = this
        this._files = {}
    }
    static Get(){
        return AssetManager.I||new AssetManager()
    }
    loadImages(paths) {
        return Promise.all(paths.map(this.loadImage.bind(this)))
    }
    loadImage(path) {
        return new Promise((resolve, reject) => {
            const image = document.createElement("img");
            image.onload = () => {
                resolve(image)
            }
            image.onerror = () => {
                reject(`image "${path}" not found`)
            }
            image.src = path;
            this._files[path] = image
        })
    }
    static File(path){
        return this.Get()._files[path]
    }
}
