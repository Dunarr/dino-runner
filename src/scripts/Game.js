import "/styles/Game.css"
import AssetManager from "./AssetManager";
import spriteSheet from "/imgs/spritesheet.png"
import background from "/imgs/background.png"
import {toggleClass} from "./utils";
import Mob from "./Mob";
import Player from "./Player";

export default class Game {
    _score
    _canvas
    _ctx
    _play
    _time
    _width
    _height
    _backgroundPos
    _mobs
    _player
    _mobInterval
    _mask
    _lastMobDelta
    _lost
    _restartPopup
    _restartPopupScore
    _scoreCounter
    constructor() {
        this._restartPopup = document.createElement("div")
        this._restartPopup.classList.add("popup")
        const popupTitle = document.createElement("h1")
        popupTitle.innerHTML = "You died!"
        const restartButton = document.createElement("div")
        restartButton.classList.add("btn")
        restartButton.innerHTML = "Try again"
        restartButton.addEventListener("click", this.start.bind(this))
        this._restartPopupScore = document.createElement("span")
        const popupScore = document.createElement("div")
        popupScore.innerHTML = "Score: "
        popupScore.appendChild(this._restartPopupScore)
        this._restartPopup.appendChild(popupTitle)
        this._restartPopup.appendChild(popupScore)
        this._restartPopup.appendChild(restartButton)

        this._scoreCounter = document.createElement("div")
        this._scoreCounter.classList.add("score-counter")


        this._mask = document.createElement("div")
        this._mask.classList.add("mask")
        this._canvas = document.createElement("canvas")
        this._canvas.width = this._width = 96
        this._canvas.height = this._height = window.innerHeight/window.innerWidth*this._width
        this._canvas.classList.add("screen")
        this._player = new Player(this._height)
        document.body.appendChild(this._restartPopup)
        document.body.appendChild(this._scoreCounter)
        document.body.appendChild(this._canvas)
        document.body.appendChild(this._mask)
        this._ctx = this._canvas.getContext("2d")
        setInterval(this.update.bind(this), 10)
        document.addEventListener("touchstart", ()=>{
            if(!this._lost){
                this.play = true
            }
        })
        document.addEventListener("touchend", evt=>{
            this.play = evt.touches.length>0
        })
        this.start()
        this.render()
    }

    set play(value) {
        this._play = value;
        toggleClass(this._mask, "hidden", this._play)
    }

    get play() {
        return this._play && !this._lost
    }

    update() {
        const newTime = Date.now()
        if(this.play){
            const delta = newTime-this._time
            const deltaPos = delta*0.2
            this._score+=delta
            this._backgroundPos+=deltaPos
            this._lastMobDelta += delta
            this._mobs = this._mobs.filter(mob => mob.y < this._height)
            this._player.update(delta)
            this._mobs.forEach(mob => {
                mob.update(delta);
                if(this._height-(mob.y+mob.height) < 16){
                    if(((this._player.x+this._player.width/2)<mob.x+mob.width ) && ((this._player.x+this._player.width/2)>mob.x)){
                        this.loose()
                    }
                }
            })
            if(this._lastMobDelta >= this._mobInterval){
                this._lastMobDelta = 0
                this._mobInterval*=0.98
                this._mobs.push(new Mob())
                if(this._mobInterval < 500){
                    this._mobInterval = 500
                }
            }
            this._scoreCounter.innerText = Math.floor(this._score/1000)
        }
        this._time = newTime;

    }
    render(){
        this._ctx.clearRect(0, 0, this._width, this._height);
        this._ctx.drawImage(AssetManager.File(background), 0, (this._backgroundPos%272)+271)
        this._ctx.drawImage(AssetManager.File(background), 0, this._backgroundPos%272)
        this._ctx.drawImage(AssetManager.File(background), 0, (this._backgroundPos%272)-271)
        this._mobs.forEach(mob => mob.draw(this._ctx))
        this._player.draw(this._ctx)
        window.requestAnimationFrame(this.render.bind(this))
    }
    start(){
        this._restartPopup.classList.add("hidden")
        this._lost = false
        this._mobs = []
        this._score = 0
        this.play = false
        this._lastMobDelta = 0
        this._mobInterval = 1000
        this._time = Date.now()
        this._backgroundPos = 0
    }
    loose(){
        this._lost = true
        this.play = false
        this._restartPopup.classList.remove("hidden")
        this._restartPopupScore.innerText = Math.floor(this._score/1000)
    }
}
