
export function toggleClass(el, classAttribute, toggle) {
    if(el !== null) {
        if (typeof toggle === "undefined") {
            el.classList.toggle(classAttribute)
        } else {
            el.classList[toggle ? 'add' : 'remove'](classAttribute)
        }
    }
}
export function toArray(ob){
    const list = []
    if(typeof ob.length == 'number'){
        for (let i = 0; i < ob.length; i++) {
            list.push(ob[i])
        }
    } else {
        Object.keys(ob).forEach(key => {
            list.push(ob[key])
        })
    }
    return list;
}
