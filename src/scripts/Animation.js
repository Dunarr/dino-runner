export default class Animation {
    _spriteSheet
    _frame;
    _originTime;

    constructor(spriteSheet, config) {
        this._spriteSheet = spriteSheet;
        this._config = config
        this.start()
    }

    start() {
        this._frame = 0
        this._originTime = 0
    }

    update(delta) {
        this._originTime+= delta
        this._frame = Math.floor(((this._originTime/1000)%1) * this._config.sprites)
    }

    draw(ctx, x, y) {
        const {
            width,
            height,
            spriteX,
            spriteY
        } = this._config
        ctx.drawImage(this._spriteSheet, spriteX + this._frame * width, spriteY, width, height, x, y, width, height)
    }
}

