import "/styles/load.css"
export default function loadScreen(){
    return new Promise(resolve => {
        window.addEventListener("touchstart", resolve)
        const dino = document.createElement("div");
        const smoke = document.createElement("div");
        const catchPhrase = document.createElement("div")
        catchPhrase.classList.add("catchphrase")
        catchPhrase.innerHTML = "<div>Bored students<br>studio</div>"
        dino.classList.add("dino")
        smoke.classList.add("smoke")
        document.body.appendChild(catchPhrase)
        document.body.appendChild(smoke)
        document.body.appendChild(dino)
        setTimeout(()=>{
            dino.remove()
            smoke.remove()
            catchPhrase.remove()
            resolve()
        }, 10500)
    })
}

export function random(from, to){
    return Math.floor(Math.random()*(to+1))+from
}

export function randomOfArray(array){
    return array[random(0, array.length-1)]
}

