import mobs from "/config/mobs.json";
import {random, randomOfArray} from "./load";
import Animation from "./Animation";
import spritesheet from "/imgs/spritesheet.png"
import AssetManager from "./AssetManager";
export default class Mob {
    x
    y
    height
    width
    animation
    constructor() {
        this._config = randomOfArray(mobs)
        this.height = this._config.height
        this.width = this._config.width
        this.y = -this._config.width
        this.x = random(16, 96-16-this._config.width)
        this.animation = new Animation(AssetManager.File(spritesheet), this._config)
    }
    update(delta){
        this.y+=delta*0.2
        this.animation.update(delta)
    }
    draw(ctx){
        this.animation.draw(ctx, this.x, this.y)
    }
}
