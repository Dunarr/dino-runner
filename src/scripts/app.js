import Game from "./Game"
import loadScreen from "./load";
import AssetManager from "./AssetManager";
import spriteSheet from "/imgs/spritesheet.png"
import background from "/imgs/background.png"

Promise.all(
    [
        loadScreen(),
        AssetManager.Get().loadImages([spriteSheet, background])
    ]).then(()=>{
new Game()
})
